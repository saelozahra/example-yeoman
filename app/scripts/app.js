'use strict';

/**
 * @ngdoc overview
 * @name exampleYeomanApp
 * @description
 * # exampleYeomanApp
 *
 * Main module of the application.
 */
angular
  .module('exampleYeomanApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngSanitize'
  ]);
