'use strict';

/**
 * @ngdoc function
 * @name exampleYeomanApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the exampleYeomanApp
 */
angular.module('exampleYeomanApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
